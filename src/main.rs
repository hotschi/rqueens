extern crate parallel_iterator;

use parallel_iterator::ParallelIterator;
use std::time::Instant;
use std::env;

const MAX_N: usize = 29;
const START_LEVEL3: usize = (MAX_N - 3) * (MAX_N - 2) * (MAX_N - 1);
const START_LEVEL4: usize = (MAX_N - 4);
const LOOK_AHEAD: u8 = 3;
const RESULTS: [u64; MAX_N] = [
	1u64,
    0u64,
    0u64,
    2u64,
    10u64,
    4u64,
    40u64,
    92u64,
    352u64,
    724u64,
    2680u64,
    14200u64,
    73712u64,
    365596u64,
    2279184u64,
    14772512u64,
    95815104u64,
    666090624u64,
    4968057848u64,
    39029188884u64,
    314666222712u64,
    2691008701644u64,
    24233937684440u64,
    227514171973736u64,
    2207893435808352u64,
    22317699616364044u64,
    234907967154122528u64,
    0,  // Not yet calculated
    0];
    
#[derive(Debug)]
enum ParseError {
	InvalidArgument,
	TooManyArguments,
	NumberOutOfRange,
}

const DEFAULT_STARTSIZE: u8 = 2;
const DEFAULT_ENDSIZE: u8 = 16;

#[derive(Default)]
struct InputArgs {
	start: u8,
	end: u8,
	single_core: bool,
}

#[derive(Default, Clone, Copy)]
struct Diagonal(u32, u32);

#[derive(Default, Clone, Copy)]
struct Level(u32, u32, u32);

fn loop1(n: u8, start_queen: Diagonal) -> (u32, Vec<Level>) {
	let mut start_level4 = vec![Level::default(); START_LEVEL4];
	let mut level4_cnt : u32 = 0;
	let mut cols: [u32; MAX_N] = [0; MAX_N];
	let mut possibs: [u32; MAX_N] = [0; MAX_N];
	let mut diag_l : [u32; MAX_N] = [0; MAX_N];
	let mut diag_r : [u32; MAX_N] = [0; MAX_N];
	let mut rest: [i8; MAX_N] = [0; MAX_N];
	let bit0: u32 = start_queen.0;
	let bit1: u32 = start_queen.1;
	let mut d: usize = 1;
	let store_level: i8 = (n - 2 - LOOK_AHEAD - 1) as i8;

	cols[d] = bit0 | bit1 | (std::u32::MAX << n);
	diag_l[d] = (bit0 << 2) | (bit1 << 1);
	diag_r[d] = (bit0 >> 2) | (bit1 >> 1);
	rest[d]  = (n - 2 - LOOK_AHEAD) as i8;

	let mut possib: u32 = cols[d] | diag_l[d] | diag_r[d];
	diag_l[d] <<= 1;
	diag_r[d] >>= 1;

	while d > 0 {
		let mut l_rest = rest[d];

		while possib != std::u32::MAX {
		
			let mut bit: u32 = !possib & (possib + 1);
			possib ^= bit;
			let new_diag_l = (bit << 1) | diag_l[d];
			let new_diag_r = (bit >> 1) | diag_r[d];
			bit |= cols[d];
			let new_possib: u32 = bit | new_diag_l | new_diag_r;

			if new_possib != std::u32::MAX {
				let lookahead1: u32 = bit | (new_diag_l << (LOOK_AHEAD - 2)) |	(new_diag_r >> (LOOK_AHEAD - 2));
				let lookahead2: u32 = bit | (new_diag_l << (LOOK_AHEAD - 1)) | (new_diag_r >> (LOOK_AHEAD - 1));
				
				if (l_rest > 0i8) && ((lookahead2 == std::u32::MAX) || (lookahead1 == std::u32::MAX)) {
					continue;
				}

				if l_rest == (store_level + 1) {
					start_level4[level4_cnt as usize].0 = bit;
					start_level4[level4_cnt as usize].1 = new_diag_l;
					start_level4[level4_cnt as usize].2 = new_diag_r;
					level4_cnt += 1;
					continue; 
				}

				l_rest -= 1;

				// The next two lines save stack depth + backtrack
				// operations when we passed the last possibility in 
				// a row. Go lower in the stack, avoid branching by 
				// writing above the current position.
				possibs[d] = possib;
				d += (possib != std::u32::MAX) as usize;
				possib = new_possib;

				cols[d] = bit;
				diag_l[d] = new_diag_l << 1;
				diag_r[d] = new_diag_r >> 1;
				rest[d] = l_rest;
			}
		}
		d -= 1;
		possib = possibs[d];
	}
	(level4_cnt, start_level4)
}

fn loop2(n: u8, start_level3: Level) -> u64 {
	let mut num: u64 = 0;
	let mut cols: [u32; MAX_N] = [0; MAX_N];
	let mut possibs: [u32; MAX_N] = [0; MAX_N];
	let mut diag_l : [u32; MAX_N] = [0; MAX_N];
	let mut diag_r : [u32; MAX_N] = [0; MAX_N];
	let mut rest: [i8; MAX_N] = [0; MAX_N];
	let mut d: usize = 1;
	let store_level: i8 = (n - 2 - LOOK_AHEAD - 1) as i8;

	cols[d] = start_level3.0;
	diag_l[d] = start_level3.1;
	diag_r[d] = start_level3.2;
	rest[d] = store_level;
	
	let mut possib: u32 = cols[d] | diag_l[d] | diag_r[d];
	diag_l[d] <<= 1;
	diag_r[d] >>= 1;

	while d > 0 {
		let mut l_rest: i8 = rest[d];

		while possib != std::u32::MAX {
			let mut bit: u32 = !possib & (possib + 1);
			possib ^= bit;
			let new_diag_l = (bit << 1) | diag_l[d];
			let new_diag_r = (bit >> 1) | diag_r[d];
			bit |= cols[d];
			let new_possib: u32 = bit | new_diag_l | new_diag_r;

			if new_possib != std::u32::MAX {
				let lookahead1: u32 = bit | (new_diag_l << (LOOK_AHEAD - 2)) |	(new_diag_r >> (LOOK_AHEAD - 2));
				let lookahead2: u32 = bit | (new_diag_l << (LOOK_AHEAD - 1)) | (new_diag_r >> (LOOK_AHEAD - 1));
				
				if (l_rest > 0i8) && ((lookahead2 == std::u32::MAX) || (lookahead1 == std::u32::MAX)) {
					continue;
				}

				possibs[d] = possib;
				d += (possib != std::u32::MAX) as usize;
				possib = new_possib;
				
				l_rest -= 1;

				cols[d] = bit;
				diag_l[d] = new_diag_l << 1;
				diag_r[d] = new_diag_r >> 1;
				rest[d] = l_rest;
			} else {
				num += (bit == std::u32::MAX) as u64;
			}
		}
		d -= 1;
		possib = possibs[d];
	}
	num
}

fn nqueens(n: u8) -> u64 {
	// counter for the number of solutions
	// sufficient until n=29 (estimated)
	let mut num: u64 = 0;

	// the toplevel is two fors, to save one bit of symmetry in the
	// enumeration by forcing second queen to be AFTER the first queen
	let mut start_cnt : u16 = 0;
	let mut start_queens: [Diagonal; (MAX_N - 2) * (MAX_N -1)] = [Diagonal::default(); (MAX_N - 2) * (MAX_N -1)];

	for q0 in 0..(n - 2)  {
		for q1 in (q0 + 2)..n {
			start_queens[start_cnt as usize].0 = 1 << q0;
			start_queens[start_cnt as usize].1 = 1 << q1;
			start_cnt += 1;
		}
	}

	let mut start_level3: [Level; START_LEVEL3] = [Level::default(); START_LEVEL3];
	let mut start_level3_cnt: u32 = 0;
	
	let n1 = n.clone();
	for ret in ParallelIterator::new(move || 0..start_cnt, move || move |i| loop1(n1, start_queens[i as usize])) {
		let (cnt, vals) = ret;
		for i in 0..cnt {
			start_level3[(i + start_level3_cnt) as usize] = vals[i as usize];
		}
		start_level3_cnt += cnt;
	}

	let n2 = n.clone();
	for ret in ParallelIterator::new(move || 0..start_level3_cnt, move || move |i| loop2(n2, start_level3[i as usize])) {
		num += ret;
	}
	num * 2
}

fn nqueens_single_core(n: u8) -> u64 {
	// counter for the number of solutions
	// sufficient until n=29 (estimated)
	let mut num: u64 = 0;

	// the toplevel is two fors, to save one bit of symmetry in the
	// enumeration by forcing second queen to be AFTER the first queen
	let mut start_cnt : u16 = 0;
	let mut start_queens: [Diagonal; (MAX_N - 2) * (MAX_N -1)] = [Diagonal::default(); (MAX_N - 2) * (MAX_N -1)];

	for q0 in 0..(n - 2)  {
		for q1 in (q0 + 2)..n {
			start_queens[start_cnt as usize].0 = 1 << q0;
			start_queens[start_cnt as usize].1 = 1 << q1;
			start_cnt += 1;
		}
	}

	let mut start_level3: [Level; START_LEVEL3] = [Level::default(); START_LEVEL3];
	let mut start_level3_cnt: u32 = 0;

	for cnt in 0..start_cnt {
		let mut start_level4 = &mut start_level3[(start_level3_cnt as usize)..((start_level3_cnt as usize) + START_LEVEL4 + 1)];
		let mut level4_cnt : u32 = 0;
		let mut cols: [u32; MAX_N] = [0; MAX_N];
		let mut possibs: [u32; MAX_N] = [0; MAX_N];
		let mut diag_l : [u32; MAX_N] = [0; MAX_N];
		let mut diag_r : [u32; MAX_N] = [0; MAX_N];
		let mut rest: [i8; MAX_N] = [0; MAX_N];
		let bit0: u32 = start_queens[cnt as usize].0;
		let bit1: u32 = start_queens[cnt as usize].1;
		let mut d: usize = 1;
		let store_level: i8 = (n - 2 - LOOK_AHEAD - 1) as i8;

		cols[d] = bit0 | bit1 | (std::u32::MAX << n);
		diag_l[d] = (bit0 << 2) | (bit1 << 1);
		diag_r[d] = (bit0 >> 2) | (bit1 >> 1);
		rest[d]  = (n - 2 - LOOK_AHEAD) as i8;

		let mut possib: u32 = cols[d] | diag_l[d] | diag_r[d];
		diag_l[d] <<= 1;
		diag_r[d] >>= 1;

		while d > 0 {
			let mut l_rest = rest[d];

			while possib != std::u32::MAX {
				let mut bit: u32 = !possib & (possib + 1);
				possib ^= bit;
				let new_diag_l = (bit << 1) | diag_l[d];
				let new_diag_r = (bit >> 1) | diag_r[d];
				bit |= cols[d];
				let new_possib: u32 = bit | new_diag_l | new_diag_r;

				if new_possib != std::u32::MAX {
					let lookahead1: u32 = bit | (new_diag_l << (LOOK_AHEAD - 2)) |	(new_diag_r >> (LOOK_AHEAD - 2));
					let lookahead2: u32 = bit | (new_diag_l << (LOOK_AHEAD - 1)) | (new_diag_r >> (LOOK_AHEAD - 1));
					
					if (l_rest > 0i8) && ((lookahead2 == std::u32::MAX) || (lookahead1 == std::u32::MAX)) {
						continue;
					}

					if l_rest == (store_level + 1) {
						start_level4[level4_cnt as usize].0 = bit;
						start_level4[level4_cnt as usize].1 = new_diag_l;
						start_level4[level4_cnt as usize].2 = new_diag_r;
						level4_cnt += 1;
						continue; 
					}

					l_rest -= 1;

					// The next two lines save stack depth + backtrack
					// operations when we passed the last possibility in 
					// a row. Go lower in the stack, avoid branching by 
					// writing above the current position.
					possibs[d] = possib;
					d += (possib != std::u32::MAX) as usize;
					possib = new_possib;

					cols[d] = bit;
					diag_l[d] = new_diag_l << 1;
					diag_r[d] = new_diag_r >> 1;
					rest[d] = l_rest;
				}
			}
			d -= 1;
			possib = possibs[d];
		}

		start_level3_cnt += level4_cnt;
	}

	for cnt in 0..start_level3_cnt {
		let mut cols: [u32; MAX_N] = [0; MAX_N];
		let mut possibs: [u32; MAX_N] = [0; MAX_N];
		let mut diag_l : [u32; MAX_N] = [0; MAX_N];
		let mut diag_r : [u32; MAX_N] = [0; MAX_N];
		let mut rest: [i8; MAX_N] = [0; MAX_N];
		let mut d: usize = 1;
		let store_level: i8 = (n - 2 - LOOK_AHEAD - 1) as i8;

		cols[d] = start_level3[cnt as usize].0;
		diag_l[d] = start_level3[cnt as usize].1;
		diag_r[d] = start_level3[cnt as usize].2;
		rest[d] = store_level;
		
		let mut possib: u32 = cols[d] | diag_l[d] | diag_r[d];
		diag_l[d] <<= 1;
		diag_r[d] >>= 1;

		while d > 0 {
			let mut l_rest: i8 = rest[d];

			while possib != std::u32::MAX {
				let mut bit: u32 = !possib & (possib + 1);
				possib ^= bit;
				let new_diag_l = (bit << 1) | diag_l[d];
				let new_diag_r = (bit >> 1) | diag_r[d];
				bit |= cols[d];
				let new_possib: u32 = bit | new_diag_l | new_diag_r;

				if new_possib != std::u32::MAX {
					let lookahead1: u32 = bit | (new_diag_l << (LOOK_AHEAD - 2)) |	(new_diag_r >> (LOOK_AHEAD - 2));
					let lookahead2: u32 = bit | (new_diag_l << (LOOK_AHEAD - 1)) | (new_diag_r >> (LOOK_AHEAD - 1));
					
					if (l_rest > 0i8) && ((lookahead2 == std::u32::MAX) || (lookahead1 == std::u32::MAX)) {
						continue;
					}

					possibs[d] = possib;
					d += (possib != std::u32::MAX) as usize;
					possib = new_possib;
					
					l_rest -= 1;

					cols[d] = bit;
					diag_l[d] = new_diag_l << 1;
					diag_r[d] = new_diag_r >> 1;
					rest[d] = l_rest;
				} else {
					num += (bit == std::u32::MAX) as u64;
				}
			}
			d -= 1;
			possib = possibs[d];
		}
	}
	num * 2
}

fn print_usage() {
	println!("Usage: r-queens BOARDSIZE");
	println!("   or: r-queens START END");
	println!("   or: r-queens -s START");
	println!("   or: r-queens START -s");
	println!("   or: r-queens -s START END");
	println!("   or: r-queens START END -s");
	println!("   or: r-queens -h");
	println!("\nThis program computes the number of solutions for the n queens problem");
	println!("(see https://en.wikipedia.org/wiki/Eight_queens_puzzle) for the board size");
	println!("BOARDSIZE or for a range of board sizes beginning with START and ending");
	println!("with END.");
	println!("\nOptions:");
	println!("   -h, --help \t\tPrint this usage information");
	println!("   -s, --single_core \tUse only one CPU-core");
}

fn parse_arguments(args: &Vec<String>) -> Result<Option<InputArgs>, ParseError> {
	let mut input_args = InputArgs{ ..Default::default() };
	
	if args.len() == 1 {
		input_args.start = DEFAULT_STARTSIZE;
		input_args.end = DEFAULT_ENDSIZE;
		input_args.single_core = false;
	} else if args.len() == 2 {
		if let Ok(val) = args[1].parse::<u8>() {
			if (val < 2) || (val > (MAX_N as u8)) {
				println!("BOARDSIZE must be between 2 and {}", MAX_N);
				return Err(ParseError::NumberOutOfRange);
			}
			input_args.start = val;
			input_args.end = val;
			
		} else {
			if (args[1] == "-h") || (args[1] == "--help") {
				print_usage();
				return Ok(None);
			} else if (args[1] == "-s") || (args[1] == "--single_core") {
				input_args.start = DEFAULT_STARTSIZE;
				input_args.end = DEFAULT_ENDSIZE;
				input_args.single_core = true;
			} else {
				println!("Invalid option {}", args[1]);
				return Err(ParseError::InvalidArgument);
			}
		}
	} else if args.len() == 3 {
		if (args[1] == "-s") || (args[1] == "--single_core") {
			input_args.single_core = true;

			if let Ok(val) = args[2].parse::<u8>() {
				if (val < 2) || (val > (MAX_N as u8)) {
					println!("START must be between 2 and {}", MAX_N);
					return Err(ParseError::NumberOutOfRange);
				}
				input_args.start = val;
				input_args.end = val;
			} else {
				println!("Invalid option {}", args[1]);
				return Err(ParseError::InvalidArgument);
			}
		} else if (args[2] == "-s") || (args[2] == "--single_core") {
			input_args.single_core = true;

			if let Ok(val) = args[1].parse::<u8>() {
				if (val < 2) || (val > (MAX_N as u8)) {
					println!("START must be between 2 and {}", MAX_N);
					return Err(ParseError::NumberOutOfRange);
				}
				input_args.start = val;
				input_args.end = val;
			} else {
				println!("Invalid option {}", args[1]);
				return Err(ParseError::InvalidArgument);
			}
		} else {
			if let Ok(val) = args[1].parse::<u8>() {
				if (val < 2) || (val > (MAX_N as u8)) {
					println!("START must be between 2 and {}", MAX_N);
					return Err(ParseError::NumberOutOfRange);
				}
				input_args.start = val;
			} else {
				println!("Invalid option {}", args[1]);
				return Err(ParseError::InvalidArgument);
			}

			if let Ok(val) = args[2].parse::<u8>() {
				if (val < input_args.start) || (val > (MAX_N as u8)) {
					println!("END must be between 2 and {}", MAX_N);
					return Err(ParseError::NumberOutOfRange);
				}
				input_args.end = val;
			} else {
				println!("Invalid option {}", args[1]);
				return Err(ParseError::InvalidArgument);
			}
		}
	} else if args.len() == 4 {
		if (args[1] == "-s") || (args[1] == "--single_core") {
			input_args.single_core = true;

			if let Ok(val) = args[2].parse::<u8>() {
				if (val < 2) || (val > (MAX_N as u8)) {
					println!("START must be between 2 and {}", MAX_N);
					return Err(ParseError::NumberOutOfRange);
				}
				input_args.start = val;
	
				if let Ok(val) = args[3].parse::<u8>() {
					if (val < input_args.start) || (val > (MAX_N as u8)) {
						println!("END must be between {} and {}", input_args.start, MAX_N);
						return Err(ParseError::NumberOutOfRange);
					}
					input_args.end = val;
				}
			}
		} else if (args[3] == "-s") || (args[3] == "--single_core") {
			input_args.single_core = true;

			if let Ok(val) = args[1].parse::<u8>() {
				if (val < 2) || (val > (MAX_N as u8)) {
					println!("START must be between 2 and {}", MAX_N);
					return Err(ParseError::NumberOutOfRange);
				}
				input_args.start = val;
	
				if let Ok(val) = args[2].parse::<u8>() {
					if (val < input_args.start) || (val > (MAX_N as u8)) {
						println!("END must be between {} and {}", input_args.start, MAX_N);
						return Err(ParseError::NumberOutOfRange);
					}
					input_args.end = val;
				}
			}
		} else {
			println!("Invalid options: {} {} {}", args[1], args[2], args[3]);
			return Err(ParseError::InvalidArgument);
		}
	} else if args.len() > 4 {
		println!("Too many arguments");
		return Err(ParseError::TooManyArguments);
	}
	Ok(Some(input_args))
}

fn main() -> Result<(), ParseError> {
	let input_args: InputArgs;
	
	let args: Vec<String> = env::args().collect();
	if let Some(vals) = parse_arguments(&args)? {
		input_args = vals;
	} else {
		return Ok(());
	}

	for n in input_args.start..=input_args.end {
		let start_time = Instant::now();
		let result = if input_args.single_core {
			nqueens_single_core(n)
		} else {
			nqueens(n)
		};
		let diff = start_time.elapsed().as_secs_f32();
		if result == RESULTS[(n as usize) - 1] {
			print!("PASSED");
		} else {
			print!("FAILED");
		}
		println!(", N={:02}, Solutions: {:18}, Expected: {:18}, Elapsed Time [sec]: {:.3}, Solutions/sec: {:.1}", 
				n, result, RESULTS[(n as usize) - 1], diff, (result as f32) / diff);
	}
		
	Ok(())
}
